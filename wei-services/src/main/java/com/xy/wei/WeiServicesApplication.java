package com.xy.wei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeiServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeiServicesApplication.class, args);
	}

}
